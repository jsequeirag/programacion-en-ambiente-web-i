<?php

namespace App\Controllers;




class CategoryController extends BaseController
{
    function getUserCategories($idUser)
    {
        $categoryModel = new \App\Models\CategoryModel();

        $result = $categoryModel->getUserCategories($idUser);

        return $result;
    }

    function getCategories()
    {
        $categoryModel = new \App\Models\CategoryModel();

        $result = $categoryModel->getCategories();

        return $result;
    }


    

  
}
