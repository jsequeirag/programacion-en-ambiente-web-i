<?php

namespace App\Controllers;




class AllNewsController extends BaseController
{

    public function getAllNews($idUser)
    {
        $allNewsModel = new \App\Models\AllNewsModel();

        $result = $allNewsModel->getAllNews($idUser);

        return $result;
    }
    public function getAllNewsByCategory($idUser, $category)
    {
        $allNewsModel = new \App\Models\AllNewsModel();

        $result = $allNewsModel->getAllNewsByCategory($idUser, $category);

        return $result;
    }

    function loadNewsToDataBase($userId)
    {
        $allNewsModel = new \App\Models\AllNewsModel();

        $userResourceController = new \App\Controllers\UserResourceController();

        $urls = $userResourceController->getUserResource($userId);


        $allNewsModel->deleteOldNews($userId);

        $allNewsModel->loadNewsToDataBase($userId, $urls);
    }
    public function loadAllNewsByResource($userId, $url)
    {
        $userResource = new \App\Models\AllNewsModel();

        $urlChanged = str_replace(".", "/", "$url");

        $url = str_replace("%7D", ".", "$urlChanged");

        $loadAllNewsByResource = $userResource->loadAllNewsByResource($userId, $url);

        $newdata = [
            'loadAllNewsByResource' => $loadAllNewsByResource,
            'idUserPublic' => $userId
        ];

        echo view('watchonlyresource',  $newdata);
    }

    public function search()
    {
        $userResource = new \App\Models\AllNewsModel();


        $word = $this->request->getGet('word');

        $loadAllNewsByResource = $userResource->search($word);

        $newdata = [
            'loadAllNewsByResource' => $loadAllNewsByResource
        ];

        echo view('search',  $newdata);
    }


    public function hashtag()
    {
        $allNewsModel = new \App\Models\AllNewsModel();

        $hashtag = $allNewsModel->hashtag();

        return $hashtag;
    }

    public function loadHashtag($hashtag)
    {
        $hashtag = str_replace(
            '%20',
            ' ',
            $hashtag
        );


        $allNewsMode = new \App\Models\AllNewsModel();

        $loadAllNewsByHashtag = $allNewsMode->loadHashtag($hashtag);

        $newdata = [
            'loadAllNewsByHashtag' => $loadAllNewsByHashtag
        ];
        echo view('hashtag',  $newdata);
    }
}
