<?php

namespace App\Controllers;

class UserResourceController extends BaseController
{
    function getUserResource($idUser)
    {
        $userResource = new \App\Models\UserResourceModel();

        $result = $userResource->getUserResource($idUser);

        return $result;
    }

    public function updateResource()
    {
        $UserResourceModel = new \App\Models\UserResourceModel();
        $userModel = new \App\Models\UserModel();
        $name = $this->request->getGet('name');
        $category = $this->request->getGet('category');
        $url = $this->request->getGet('url');
        $idUser = $this->request->getGet('idUser');

        $oldName = $this->request->getGet('oldName');
        $oldCategory = $this->request->getGet('oldCategory');
        $oldUrl = $this->request->getGet('oldUrl');




        $UserResourceModel->updateResource($idUser, $name, $oldName, $category, $oldCategory, $url, $oldUrl);

        $profile = $userModel->getProfile($idUser);

        $newdata = [
            'profile'     => $profile

        ];

        return view('cover', $newdata);
    }


    function insertUserResource()
    {
        $userResource = new \App\Models\UserResourceModel();
        $idUser = $this->request->getGet('idUser');
        $url = $this->request->getGet('url');
        $name = $this->request->getGet('name');
        $category = $this->request->getGet('category');


        $resourceExisted = $userResource->getOnlyResource($idUser, $url);

        if (!$resourceExisted) {

            try {
                simplexml_load_file($url);
                $userResource->insertUserResource($idUser, $url, $name, $category);
                $userModel = new \App\Models\UserModel();

                $profile = $userModel->getProfile($idUser);

                $newdata = [
                    'profile'     => $profile

                ];

                echo view('cover', $newdata);
            } catch (\exception $e) {

                echo "<script type='text/javascript'>alert('Oooops!');</script>";

                $userModel = new \App\Models\UserModel();

                $profile = $userModel->getProfile($idUser);

                $newdata = [
                    'profile'     => $profile

                ];

                echo view('cover', $newdata);
            }
        } else {

            $userModel = new \App\Models\UserModel();

            $profile = $userModel->getProfile($idUser);

            $newdata = [
                'profile'     => $profile

            ];

            echo view('cover', $newdata);
        }
    }
    public function deleteResource($name, $idUser)
    {
        $userResource = new \App\Models\UserResourceModel();

        $userResource->deleteResource($name);

        $userModel = new \App\Models\UserModel();

        $profile = $userModel->getProfile($idUser);

        $newdata = [
            'profile'     => $profile

        ];

        echo view('cover', $newdata);
    }
}
