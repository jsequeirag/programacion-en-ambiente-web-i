<?php




namespace App\Controllers;

require "PHPMailer/Exception.php";
require "PHPMailer/PHPMailer.php";
require "PHPMailer/SMTP.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class UserController extends BaseController
{


    public function prueba()
    {
        $data = [
            'mensaje' => "this user doesnt exist"
        ];

        echo view('cover', $data);
    }

    public function loginValidate()
    {
        $userModel = new \App\Models\UserModel();
        $userResource = new \App\Models\UserResourceModel();



        // $session->destroy();
        //$session->set($newdata);

        // var_dump(var_dump( (array) $session));

        $email = $this->request->getGet('email');

        $password = $this->request->getGet('password');


        $validate = $userModel->getUser($email, $password);

        foreach ($validate as $user) {

            $newdata = [
                'id'     => $user->id,
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'email' => $user->email,
                'password' => $user->password,
                'rol' => $user->rol
            ];
        }

        if ($validate) {

            $session = session();

            $session->set($newdata);

            $resource = $userResource->getUserResource($newdata['id']);

            if ($resource) {

                $profile = $userModel->getProfile($newdata['id']);

                $data = [

                    'profile' =>  $profile
                ];

                return  view('cover', $data);
            } else {
                echo  view('suggestion');
            }
        } else {

            $data = [
                'mensaje' => "this user doesnt exist"
            ];

            echo view('login', $data);
        }
    }

    public function emailValidate()
    {
        $email = $this->request->getGet('email');
        $userModel = new \App\Models\UserModel();
        $emailValidate = $userModel->getOnlyUser($email);
        if ($emailValidate) {
            $data = [
                'mensaje' => "This email is already registered"
            ];

            echo view('register', $data);
        } else {


            /* -------------------------------------------------------------------------- */
            /*                                Genera codigo                               */
            /* -------------------------------------------------------------------------- */

            mt_srand(time());
            $code = mt_rand(0, 999);

            /* -------------------------------------------------------------------------- */
            /*                                envia correo                                */
            /* -------------------------------------------------------------------------- */

            self::sentEmail($code, $email);

            $data = [
                'code' => $code,
            ];

            echo view('register', $data);
        }
    }


    public static function sentEmail($code, $email)
    {
        $mail = new PHPMailer;
        $mail->isSMTP();                            // Establecer el correo electrónico para utilizar SMTP
        $mail->Host = 'smtp.gmail.com';             // Especificar el servidor de correo a utilizar 
        $mail->SMTPAuth = true;                     // Habilitar la autenticacion con SMTP
        $mail->Username = 'joseluissequeirag@gmail.com';          // Correo electronico saliente ejemplo: tucorreo@gmail.com
        $mail->Password = '2141996JoSegoogle';         // Tu contraseña de gmail
        $mail->SMTPSecure = 'tls';                  // Habilitar encriptacion, `ssl` es aceptada
        $mail->Port = 587;                          // Puerto TCP  para conectarse 
        $mail->setFrom('joseluissequeirag@gmail.com', 'joseluissequeirag@gmail.com'); //Introduzca la dirección de la que debe aparecer el correo electrónico. Puede utilizar cualquier dirección que el servidor SMTP acepte como válida. El segundo parámetro opcional para esta función es el nombre que se mostrará como el remitente en lugar de la dirección de correo electrónico en sí.
        $mail->addReplyTo("joseluissequeirag@gmail.com", "joseluissequeirag"); //Introduzca la dirección de la que debe responder. El segundo parámetro opcional para esta función es el nombre que se mostrará para responder
        $mail->addAddress("$email");   // Agregar quien recibe el e-mail enviado

        $mail->isHTML(true);  // Establecer el formato de correo electrónico en HTML

        $mail->Subject = 'Feed CODE';
        $mail->msgHTML($code);
        if (!$mail->send()) {
            echo '<p style="color:red">No se pudo enviar el mensaje..';
            echo 'Error de correo: ' . $mail->ErrorInfo;
            echo "</p>";
            return true;
        } else {
            echo '<p style="color:green">Tu mensaje ha sido enviado!</p>';
            return false;
        }
    }

    public function validateCode()
    {

        $emailCode = $this->request->getGet('emailCode');
        $insertedCode = $this->request->getGet('insertedCode');
        if ($emailCode == $insertedCode) {
            mt_srand(time());

            $idShare = mt_rand(1000, 9999);

            echo  $idShare;

            echo 'register';
            $userModel = new \App\Models\UserModel();

            $firstName = $this->request->getGet('firstName');
            $lastName = $this->request->getGet('lastName');
            $email = $this->request->getGet('email');
            $password = $this->request->getGet('password');
            $rol = 'user';
            // $emailValidate = $userModel->getOnlyUser($email);
            $userModel->InsertUser($firstName, $lastName, $email, $password, $rol, $idShare);

            // $validate = $userModel->getUser($email, $password);

            // foreach ($validate as $user) {

            //     $newdata = [
            //         'id'     => $user->id,
            //         'firstName' => $user->first_name,
            //         'lastName' => $user->last_name,
            //         'email' => $user->email,
            //         'password' => $user->password,
            //         'rol' => $user->rol
            //     ];
            // }

            // $session = session();

            // $session->set($newdata);

            return  view('login');
        } else {
            $data = [
                'modalMsj' => 'Invalid code',
            ];
            echo view('register', $data);
        }
    }



    public function register()
    {

        mt_srand(time());

        $idShare = mt_rand(1000, 9999);

        $userModel = new \App\Models\UserModel();

        $firstName = $this->request->getGet('firstName');
        $lastName = $this->request->getGet('lastName');
        $email = $this->request->getGet('email');
        $password = $this->request->getGet('password');
        $rol = 'user';
        $emailValidate = $userModel->getOnlyUser($email);

        if ($emailValidate) {
            $data = [
                'mensaje' => "This email is already registered"
            ];

            echo view('register', $data);
        } else {
            $userModel->InsertUser($firstName, $lastName, $email, $password, $rol, $idShare);

            $validate = $userModel->getUser($email, $password);

            foreach ($validate as $user) {

                $newdata = [
                    'id'     => $user->id,
                    'firstName' => $user->first_name,
                    'lastName' => $user->last_name,
                    'email' => $user->email,
                    'password' => $user->password,
                    'rol' => $user->rol
                ];
            }

            $session = session();

            $session->set($newdata);

            $profile = $userModel->getProfile($newdata['id']);

            $data = [

                'profile' =>  $profile
            ];

            return  view('cover', $data);
        }
    }


    public  function updateProfile($idUser)
    {
        $userModel = new \App\Models\UserModel();

        $updateProfile = $userModel->updateProfile($idUser);

        $newdata = [
            'profile' => $updateProfile

        ];

        echo view('cover', $newdata);
    }

    public  function redirectCover($idUser)
    {
        $userModel = new \App\Models\UserModel();

        $profile = $userModel->getProfile($idUser);

        $newdata = [
            'profile'     => $profile

        ];

        echo view('cover', $newdata);
    }

    public  function getOnlyPublicProfile()
    {
        $userModel = new \App\Models\UserModel();

        $profiles = $userModel->getOnlyPublicProfile();

        $newdata = [
            'profiles'     => $profiles
        ];

        echo view('publicdashboard', $newdata);
    }

    public function publicCover($user)
    {
        $idUserPublic = $user;

        $newdata = [
            'idUserPublic'     => $idUserPublic
        ];

        echo view('publiccover', $newdata);
    }
    public  function getUserById($idUser)
    {
        $userModel = new \App\Models\UserModel();

        $user = $userModel->getUserById($idUser);

        return $user;
    }
}
