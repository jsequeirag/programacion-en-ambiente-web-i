<?php

namespace App\Controllers;

class Index extends BaseController
{
	public function index()
	{
		return view('index');
	}

	public function login()
	{
		return view('login');
	}
	public function register()
	{
		return view('register');
	}

	public function cover()
	{
		return view('cover');
	}

	public function logout()
	{

		$session = session();

		$session->destroy();

		$newdata = [
			'destroySession'     => true
		];

		return view('index', $newdata);
	}

	public function publicCover($user)
	{
		$idUserPublic = $user;

		$newdata = [
			'idUserPublic'     => $idUserPublic
		];

		echo view('publiccover', $newdata);
	}
}
