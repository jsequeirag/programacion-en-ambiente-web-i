<?php
/*include_once 'loginfunc.php';

if ($_GET["action"] == "login") {

  $email = $_GET['email'];

  $password = $_GET['password'];

  $result = loginUser($email, $password);

  if ($result) {
    session_start();
    $_SESSION['user'] = $result;
    if ($result["rol"] == 'admi') {
      
      header("location:edit.php");
   
    }
    if ($result['rol'] == 'user') {
      $user= $_SESSION['user'];
      $id=$user['id'];
      $resources=getAllResourcesUrl($id);

      if($resources){
      header("location:cover.php?action=load");
    }else{
      header("location:suggestion.php");
    }
    }
  } else {
    header("location:login.php?action=message");
  }
}
*/
$isTouch = isset($mensaje);


if(!$isTouch){
 $mensaje='';
}


?>

<!-- ----------------------------------------------------------------------- -->
<!--                                  HTML                                   -->
<!-- ----------------------------------------------------------------------- -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="<?php echo base_url('css/login.css') ?>" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="<?php echo base_url('index') ?>">
        <img src="<?php echo base_url('img/logo2.png') ?>" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <a href="/index/register">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Register
              </button>
            </a>

          </div>

        </ul>
      </div>
    </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>


    <div class="container-form mt-1 mb-1 ">

      <form method="Get" action="/UserController/loginValidate" class="border p-5 border-secondary rounded">
       
          <p class="text-center text-danger"><?php echo $mensaje ?></p>
      

        <!-- Email input -->
        <div class="form-outline mb-4">
          <input type="email" id="form2Example1" class="form-control" name="email" required />

          <label class="form-label" for="form2Example1">Email address</label>
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
          <input type="password" id="form2Example2" class="form-control" name="password" required />
          <label class="form-label" for="form2Example2">Password</label>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-secondary btn-block mb-4">Sign in</button>

        <div class="col">
          <!-- Simple link -->
          <p class="text-center">if you dont have account,
            <a text-center href="/index/register">signup here</a>
          </p>
        </div>
      </form>
    </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="text-center text-white" style="background-color:#E0E0E0">
    <!-- Grid container -->
    <div class="container p-4"></div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-white p-3" style="background-color: #757575">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->


  <!-- ----------------------------------------------------------------------- -->
  <!--                                  JAVASCRIPT                             -->
  <!-- ----------------------------------------------------------------------- -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  JAVASCRIPT                             -->
  <!-- ----------------------------------------------------------------------- -->
 
</body>

</html>