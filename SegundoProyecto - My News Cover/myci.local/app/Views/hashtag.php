<?php


$session =  session();
$firstName = $session->firstName;
$lastName = $session->lastName;
$idUser = $session->id;


foreach ($loadAllNewsByHashtag as $new) {
    $hashtag = $new->categories;
    break;
}

/* -------------------------------------------------------------------------- */
/*                  carga datos dl usuario del perfil  public                 */
/* -------------------------------------------------------------------------- */



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <link rel="stylesheet" href="<?php echo base_url('css/watchonlyresource.css') ?>" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 HEADER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <header>

        <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="index.php">
                <img src="<?php echo base_url('img/logo2.png') ?>" width="130" height="70" class="d-inline-block align-top" alt="" />
            </a>



            <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
                <ul class="navbar-nav me-5">

                    <?php
                    if ($firstName) :
                    ?>
                        <div class="btn-group me-4">
                            <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
                                <?php echo $firstName;
                                echo ' ';
                                echo $lastName; ?>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">

                                <li><a class="dropdown-item text-center " href="/index/logout">Log Out</a></li>

                            </ul>

                        </div>
                    <?php
                    endif;
                    ?>

                </ul>
            </div>

        </nav>

    </header>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                  MAIN                                   -->
    <!-- ----------------------------------------------------------------------- -->

    <main>

        <div class="container-tab rounded border mt-4 mb-4">

            <ul class="nav nav-tabs nav-fill mb-3 border-bottom" id="ex1" role="tablist">

                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="main1" data-mdb-toggle="tab" href="#main" role="tab" aria-controls="ex2-tabs-2" aria-selected="false"><?php echo '#' . $hashtag ?></a>
                </li>


            </ul>
            <!-- Tabs navs -->

            <!-- Tabs content -->
            <div class="tab-content" id="ex2-content">



                <div class="tab-pane fade show active" id="main" role="tabpanel">
                    <div class="float-start d-flex flex-column ms-3 ">
                        <?php
                        if ($firstName) :
                        ?>
                            <a href="/userController/redirectCover/<?php echo $idUser  ?>" class="align-self-start">
                                <button type="button" data-mdb-toggle="modal" data-mdb-target="#modalinsert" class="btn btn-secondary ms-3 me-5 mt-1 w-75">
                                    Home
                                </button>
                            </a>

                        <?php
                        else :
                        ?>
                            <a href="/UserController/publicCover/<?php echo $idUserPublic ?>">
                                <button type="button" size="sm" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                                    Back
                                </button>
                            </a>
                        <?php
                        endif;
                        ?>
                    </div>


                    <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example">
                        <?php
                        foreach ($loadAllNewsByHashtag as $new) :
                        ?>
                            <div class="card border mb-1">

                                <?php
                                if (substr($new->img_url, -3) == 'mp4') :
                                ?>
                                    <img src="http://rafikisafari.com/wp/wp-content/themes/salient/img/no-video-img.png" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                else :
                                ?>
                                    <img src="<?php echo $new->img_url ?>" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                endif;
                                ?>
                                <div class="card-body mt-1 ms-1">
                                    <h5 class="card-title"> <?php echo $new->title ?></h5>
                                    <h6 class="card-title">Category: <?php echo $new->categories ?></h6>
                                    <h7 class="card-title">Source: <?php echo $new->source_name ?></h7>
                                    <br>
                                    <h8 class="card-title">Date: <?php echo $new->date ?></h8>
                                    <p class="card-text">
                                        <?php echo $new->description ?>
                                    </p>
                                    <a href="<?php echo $new->permalink ?>" class="btn btn-secondary">Visit Website</a>
                                </div>
                            </div>



                        <?php
                        endforeach;
                        ?>

                    </div>

                </div>

            </div>


        </div>


        </div>
    </main>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 FOOTER                                  -->
    <!-- ----------------------------------------------------------------------- -->
    <footer class="text-center text-white" style="background-color:#E0E0E0">
        <!-- Grid container -->
        <div class="container p-4"></div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-white p-3" style="background-color: #757575">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- MDB -->

    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

</body>


</html>