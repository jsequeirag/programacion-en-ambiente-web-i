<?php


$session =  session();

$firstName = $session->firstName;
$lastName = $session->lastName;
$idUser = $session->id;

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <link rel="stylesheet" href="<?php echo base_url('css/cover.css') ?>" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />

</head>

<body>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 HEADER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <header>
        <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="<?php echo base_url('index') ?>">
                <img src="<?php echo base_url('img/logo2.png') ?>" width="130" height="70" class="d-inline-block align-top" alt="" />
            </a>

            <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
                <?php
                if ($firstName) :
                ?>
                    <ul class="navbar-nav me-5">

                        <div class="btn-group me-4">
                            <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
                                <?php echo $firstName;
                                echo ' ';
                                echo $lastName; ?>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">

                                <li><a class="dropdown-item text-center " href="/index/logout">Log Out</a></li>

                            </ul>

                        </div>

                    </ul>
                <?php
                endif;
                ?>
        </nav>
    </header>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                  MAIN                                   -->
    <!-- ----------------------------------------------------------------------- -->

    <main class="d-flex justify-content-start">
        <?php
        if ($firstName) :
        ?>
            <a href="/userController/redirectCover/<?php echo $idUser  ?>" class="align-self-start">
                <button type="button" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                    Home
                </button>
            </a>
        <?php
        else :
        ?>
            <a href="/index/index" class="align-self-start">
                <button type="button" class="btn btn-warning ms-3 me-5 mt-5 w-75">
                    back
                </button>
            </a>
        <?php
        endif;
        ?>
        <div class="container mt-5">
            <table class="table align-middle ">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Ver</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($profiles as $profile) :
                    ?>
                        <tr>
                            <td><?php echo $profile->id ?></td>
                            <td><?php echo $profile->first_name . ' ' . $profile->last_name ?></td>
                            <td>
                                <a href="/UserController/publicCover/<?php echo $profile->id ?>">
                                    <button type="button" class="btn btn-secondary btn-sm px-3">
                                        <i class="fas fas fa-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>

                    <?php
                    endforeach;
                    ?>

                </tbody>
            </table>
        </div>

    </main>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 FOOTER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <footer class="text-center text-white" style="background-color:#E0E0E0">
        <!-- Grid container -->
        <div class="container p-4"></div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-white p-3" style="background-color: #757575">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- MDB -->
    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <script type="text/javascript" src="<?php echo base_url('css/cover.js') ?>"> </script>


    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

</body>


</html>