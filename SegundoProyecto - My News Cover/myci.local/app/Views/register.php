<?php
// include_once 'functions.php';

/* -------------------------------------------------------------------------- */
/*                           INSERT INTO USER TABLE                           */
/* -------------------------------------------------------------------------- */
// if ($_GET) {
//   $email = $_GET["email"];
//   $user = getOnlyUser($email);

//   if ($user) {

//     header("location:register.php?action=message");
//   } else {

//     $firstName = $_GET["firstname"];
//     $lastName = $_GET["lastname"];
//     $email = $_GET["email"];
//     $password = $_GET["password"];
//     echo $firstName, $lastName, $email, $password;
//     $sql_agregar = "INSERT INTO user (id, first_name, last_name, email, password, rol) VALUES (NULL, ?, ?, ?, ?, ?)";
//     $sentencia_agregar = conexionCover()->prepare($sql_agregar);
//     $state = $sentencia_agregar->execute(array($firstName, $lastName, $email, $password, "user"));

//     echo var_dump($state);
//     if ($state) {
//       header("Location:login.php");
//       echo 'agregado';
//     }
//   }
// }


$isTouch = isset($mensaje);


if (!$isTouch) {
  $mensaje = '';
}

$isTouch = isset($code);


if (!$isTouch) {
  $code = '';
} else {
  echo $code;
}

$isTouch = isset($modalMsj);


if (!$isTouch) {
  $modalMsj = '';
} else {
  echo $modalMsj;
}



if ($_GET) {
  $firstName = $_GET['firstName'];
  $lastName = $_GET['lastName'];
  $email = $_GET['email'];
  $password = $_GET['password'];
  $action = $_GET['action'];
} else {
  $firstName = '';
  $lastName = '';
  $email = '';
  $password = '';
  $action = '';
}





?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="<?php echo base_url('css/register.css'); ?>" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->


  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src=<?php echo base_url("img/logo2.png") ?> width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <a href="/index/login">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Log in
              </button>
            </a>

          </div>

        </ul>
      </div>
    </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>
    <?php
    if (!$mensaje == '') :
    ?>
      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <?php
    elseif ($action == 'confirm') :

      ?>

        <div class="modal fade show" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" style="padding-right: 10px; display: block;" aria-modal="true" role="dialog">

        <?php
      else :

        ?>
          <?php $mensaje = '' ?>
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <?php
        endif;
          ?>
          <!-- Modal show-->

          <!-- Modal -->

          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Code</h5>
                <p class="text-center text-danger"><?php echo $modalMsj ?></p>
                <a href="/index/register?firstName=<?php echo $firstName ?>&lastName= <?php echo $lastName ?>&email=<?php echo $email ?>&password=<?php echo $password ?>&action=''">
                  <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                </a>
              </div>
              <div class="modal-body">

                <form method="get" action="/userController/validateCode">

                  <input hidden type="text" id="form3Example1" value="<?php echo $firstName ?>" class="form-control" name="firstName" required="required" />
                  <input hidden type="text" id="form3Example2" value="<?php echo $lastName ?>" class="form-control" name="lastName" required="required" />
                  <input hidden type="email" id="form3Example3" value="<?php echo $email ?>" class="form-control" name="email" required="required" />
                  <input hidden type="password" id="form3Example4" value="<?php echo $password ?>" class="form-control" name="password" minlength="8" required="required" />
                  <input hidden id="form3Example5" value="confirm" class="form-control" name="action" minlength="8" />
                  <input hidden type="text" id="form3Example1" value="<?php echo $code ?>" class="form-control" name="emailCode" required="required" />
                  <div class=" container form-outline mb-4">

                    <input type="text" id="form3Example2" v class="form-control" name="insertedCode" required="required" />
                    <label class="form-label" for="form3Example3">Code</label>
                  </div>
                  <div class="container d-flex justify-content-center">
                    <a href="/index/register?firstName=<?php echo $firstName ?>&lastName= <?php echo $lastName ?>&email=<?php echo $email ?>&password=<?php echo $password ?>&action=''">
                      <button type="button" class="btn btn-secondary me-5" data-mdb-dismiss="modal">
                        Close
                      </button>
                    </a>

                    <button type="submit" class="btn btn-primary">Confirm</button>

                  </div>



                </form>

              </div>
              <div class="modal-footer">



              </div>
            </div>
          </div>
          </div>
          <div class="container-form mt-2 mb-2">

            <form method="GET" action="/userController/emailValidate" class="border p-5 border-secondary rounded">


              <!-- 2 column grid layout with text inputs for the first and last names -->

              <p class="text-center text-danger"><?php echo $mensaje ?></p>



              <!-- first input -->
              <div class="form-outline mb-4">
                <input type="text" id="form3Example1" value="<?php echo $firstName ?>" class="form-control" name="firstName" required="required" />
                <label class="form-label" for="form3Example3">First name</label>
              </div>



              <!-- Lastname input -->
              <div class="form-outline mb-4">
                <input type="text" id="form3Example2" value="<?php echo $lastName ?>" class="form-control" name="lastName" required="required" />
                <label class="form-label" for="form3Example3">Last name</label>
              </div>





              <!-- Email input -->
              <div class="form-outline mb-4">
                <input type="email" id="form3Example3" value="<?php echo $email ?>" class="form-control" name="email" required="required" />
                <label class="form-label" for="form3Example3">email</label>
              </div>

              <!-- Password input -->
              <div class="form-outline mb-4">
                <input type="password" id="form3Example4" value="<?php echo $password ?>" class="form-control" name="password" minlength="8" required="required" />
                <label class="form-label" for="form3Example4">password</label>

              </div>
              <div class="col">
                <!-- Simple link -->
                <p class="text-center">if you have account,
                  <a text-center href="/index/login">log in here</a>
                </p>
              </div>
              <!-- Submit button -->
              <?php
              if (!$_GET) :
              ?>
                <input name="action" value="confirm" hidden />
                <button active type="submit" class="btn btn-secondary btn-block mb-4" data-mdb-toggle="modal" data-mdb-target="#exampleModal">Register</button>
              <?php
              else :
              ?>
                <input name="action" value="confirm" hidden />
                <button type="submit" class="btn btn-secondary btn-block mb-4">Register</button>

              <?php
              endif;
              ?>


            </form>
          </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="text-center text-white" style="background-color:#E0E0E0">
    <!-- Grid container -->
    <div class="container p-4"></div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-white p-3" style="background-color: #757575">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->


  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
  <script src="<?php echo base_url('javascript/register.js') ?>"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>