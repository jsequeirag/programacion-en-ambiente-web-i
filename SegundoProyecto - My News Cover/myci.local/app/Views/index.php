<?php


// if ($idUser) {
//   header("Location: /userController/redirectCover/$idUser");
//   die();
// }

$validate = isset($destroySession);


if (!$validate) {
  echo 'no existe';
  $session = session();
  $idUser = $session->get('id');
  if ($idUser) {
    header("Location: /userController/redirectCover/$idUser");
    die();
  }
}




// if ($_GET['action'] == 'logout') {
//   session_start();
//   session_destroy();
// } else {

//   $session = $_SESSION['user'];


//   if ($session['rol'] == 'admi') {
//     header('Location:addcategory.php');
//   }
//   if ($session['rol'] == 'user') {
//     header('Location:cover.php?action=load');
//   }
// }

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>

  <link rel="stylesheet" href="<?php echo base_url('css/index.css'); ?>" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />

  <link rel="<?php echo base_url('javascript/JParticles-master/samples/css/style.css'); ?>">
  <script src="<?php echo base_url('javascript/JParticles-master/production/jparticles.js'); ?>"></script>
  <script src="<?php echo base_url('javascript/JParticles-master/production/particle.js'); ?>"></script>
  <script src="<?php echo base_url('javascript/JParticles-master/samples/js/event.js'); ?>"></script>

</head>


<body>
  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="/index/index">
        <img src=<?php echo base_url("img/logo2.png") ?> width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
          <div class="btn-group me-4">
            <a href="/index/login">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Log in
              </button>
            </a>

          </div>
          <div class="btn-group me-4">
            <a href="/index/register">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Register
              </button>
            </a>
          </div>
        </ul>
      </div>
    </nav>
    <div class="text-center mt-5">
      <div class=" d-flex justify-content-center align-items-center h-100">
        <div class="black-white">
          <h1 class="mb-3">Stay informed</h1>
          <h4 class="mb-3">Stay updated</h4>
          <a href="/userController/getOnlyPublicProfile">
            <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
              Explore
            </button>
          </a>

        </div>
      </div>
      <div class="text-white text-center bg-image" style="
          background-image: url('<?php echo base_url('img/main.png') ?>');
          background-size:1000px 400px;
          background-repeat:repeat;
          height:200px;
          background-position: center-botton;
        ">
      </div>
    </div>
  </header>

  <main class="border-bottom">
    <div class="container">
      <div class=" child-container bg-image d-flex justify-content-between align-items-center border-bottom" style="
            background-image: url('<?php echo base_url('img/devices1.png') ?>');
            height: 400px;
            width: 1900px;
            background-size:550px 300px;
            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
          ">





        <div class="padre  h-200 " id="instance">
          <div class="demo w-100">

          </div>
          <div class="uno d-flex text-center w-100 h-100 align-items-center justify-content-center">
            <div class="ml-5">
              <h1 class="mb-3 ">Anywhere</h1>
              <h4 class="mb-3">mobile App</h4>
              <a href="">
                <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                  Download
                </button>
              </a>

            </div>
          </div>
        </div>
        <script>
          bind('#instance', function() {
            return new JParticles.particle('#instance .demo', {
              color: '#7E57C2',
              range: 100000,
              proximity: 50,
              parallax: true
            });
          });
        </script>

      </div>

      <div class=" child-container  bg-image d-flex justify-content-end align-items-center " style="
            background-image: url('<?php echo base_url('img/security.png') ?>');
            height: 400px;
            width: 1900px;
            background-size:550px 300px;
            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
          ">

        <div class="padre h-200 " id="instance1">
          <div class="demo w-100">

          </div>
          <div class="uno d-flex text-center w-100 h-100 align-items-center justify-content-center">
            <div class="ml-5">
              <h1 class="mb-3 ">Security</h1>
              <h4 class="mb-3">Feed is a security plataform</h4>
              <a href="">
                <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                  Download
                </button>
              </a>

            </div>
          </div>
        </div>
        <script>
          bind('#instance1', function() {
            return new JParticles.particle('#instance1 .demo', {
              color: '#7E57C2',
              range: 3000,
              proximity: 50,
              parallax: true
            });
          });
        </script>
      </div>
    </div>
  </main>

  <footer class="text-center text-white" style="background-color:#E0E0E0">
    <!-- Grid container -->
    <div class="container p-4"></div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-white p-3" style="background-color: #757575">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>


  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>


</html>