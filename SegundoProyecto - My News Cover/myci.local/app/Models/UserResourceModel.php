<?php

namespace App\Models;


use CodeIgniter\Model;

class UserResourceModel extends Model
{
    function getUserResource($userId)
    {
        $db = db_connect();

        $sql = "SELECT * FROM user_resource WHERE user_id=?";

        $query = $db->query($sql, [$userId]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }
    function getOnlyResource($userId, $rrsLink)
    {
        $db = db_connect();

        $sql = "SELECT * FROM user_resource WHERE user_id=? AND url=?";

        $query = $db->query($sql, [$userId, $rrsLink]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }
    function insertUserResource($idUser, $url, $name, $category)
    {
        $db = db_connect();

        $sql = "INSERT INTO user_resource (user_id, url,name, category) VALUES (?, ?, ?, ?)";

        $query = $db->query($sql, [$idUser, $url, $name, $category]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }

    function updateResource($idUser, $name, $oldName, $category, $oldCategory, $url, $oldUrl)
    {
        $db = db_connect();

        $db->query("UPDATE  user_resource SET name = '$name'  WHERE name = '$oldName' AND user_id='$idUser'");

        $db->query("UPDATE  user_resource SET category = '$category'  WHERE category= '$oldCategory' AND user_id='$idUser'");

        $db->query("UPDATE  user_resource SET url = '$url'  WHERE url = '$oldUrl'  AND user_id='$idUser' ");
    }

    public function deleteResource($name)
    {
        $db = db_connect();

        $sql = "DELETE FROM user_resource WHERE name=?;";

        $query = $db->query($sql, [$name]);
    }
}
