<?php

namespace App\Models;


use CodeIgniter\Model;

class AllNewsModel extends Model
{
    function getAllNews($userId)
    {
        $db = db_connect();

        $sql = "SELECT * FROM all_news WHERE user_id = ?  order by date DESC limit 30";

        $query = $db->query($sql, [$userId]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }
    function getAllNewsByCategory($userId, $category)
    {
        $db = db_connect();

        $sql = "SELECT * FROM all_news WHERE user_id = ? and category=? order by date DESC limit 20";

        $query = $db->query($sql, [$userId, $category]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }

    public function deleteOldNews($userId)
    {
        $db = db_connect();

        $sql = "DELETE  FROM all_news WHERE user_id= ?";

        $query = $db->query($sql, [$userId]);
    }

    public static function insertNews($userId, $source_name, $category, $resource, $title, $description, $permalink, $date, $img_url, $categories)
    {
        $db = db_connect();
        $sql = 'INSERT INTO all_news (user_id,source_name ,category, resource, title, description, permalink, date, img_url, categories) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?);';
        $query = $db->query($sql, [$userId, $source_name, $category, $resource, $title, $description, $permalink, $date, $img_url, $categories]);
    }

    public function loadNewsToDataBase($userId, $urls)
    {



        foreach ($urls as $urlkey) {
            $i = 0;

            $urlResourse =  $urlkey->url;

            $categoryUser = $urlkey->category;
            try {
                $rss = simplexml_load_file($urlResourse);
            } catch (\exception $e) {
                echo "<script type='text/javascript'>alert('Oooops!');</script>";
            }


            // foreach ($rss->channel->item as $item) {

            //$media_content = $item->children($namespaces['media']);

            //  foreach ($media_content->content->thumbnail as $i) {
            //   var_dump((string)$i->attributes()->url);
            //   echo $i;
            // }


            // $namespaces = $rss->getNamespaces(true);
            // foreach ($rss->channel->item as $item) {
            //   $media_content =$item->children($namespaces['media']);

            //   foreach ($media_content->content->thumbnail as $i) {
            //     var_dump((string)$i->attributes()->url);
            //     echo $i;
            //   }

            //   echo 'asd';
            // }


            foreach ($rss->channel as $channel) {
                $source_name = $channel->title;
            }
            $nume = 0;
            foreach ($rss->channel->item as $item) {
                $link = $item->link;  //extrae el Permanklink
                $title = $item->title;  //extrae el titulo
                $date = $item->pubDate;  //extrae la fecha

                $url_imagen = "";



                $namespaces = $item->getNamespaces(true);

                $v = isset($namespaces['media']);


                if ($v) {

                    $media_content = $item->children($namespaces['media']);



                    if (is_array($media_content)) {

                        $media_content = $item->children($namespaces['media']);

                        foreach ($media_content->content->thumbnail as $i) {

                            $r = $i->attributes()->url;

                            $url_imagen = $r;
                        }
                    }
                }






                if (!$url_imagen) {
                    $url_imagen = $item->enclosure['url']; //extrae el link de la imagen 
                }
                if (!$url_imagen) {
                    $url_imagen = $item->children('media', true)->content->attributes(); //extrae link de imagen
                }
                try {

                    if (!$url_imagen) {
                        $url_imagen = "http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png";
                    }
                } catch (\Exception $e) {
                    $url_imagen = "http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png";
                }

                $categoryNew = $item->category; //extrae categoria
                $guid = $item->guid; //extrae LINK noticia original categoria
                $dateMySqlFormat =  date('Y-m-d H:i:s', strtotime($date)); // convierte la fecha al formato mySql
                $description = strip_tags($item->description);
                /*     echo $dateMySqlFormat;
                      echo $date; */
                $description = strip_tags($item->description);  //extrae la descripcion
                if (strlen($description) > 200) { //limita la descripcion a 400 caracteres
                    $stringCut = substr($description, 0, 200);
                    $description = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                }




                $db = db_connect();
                $sql = 'INSERT INTO all_news (user_id,source_name ,category, resource, title, description, permalink, date, img_url, categories) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?);';
                $query = $db->query($sql, [$userId, $source_name, $categoryUser, $urlResourse, $title, $description, $guid, $dateMySqlFormat, $url_imagen, $categoryNew]);

                /*   if ($i < 16) { // extrae solo 16 items
                          echo '<div class="cuadros1"><h4><a href="' . $link . '" target="_blank">' . $title . '</a></h4><br><img src="' . $url_imagen . '"><br>' . $description . '<br><div class="time">' . $date . '</div></div>';
                      }
                      $i++;  */
            }
        }

        return $urls;
    }
    public function loadAllNewsByResource($user_id, $name)
    {
        $db = db_connect();

        $sql = 'SELECT * FROM all_news WHERE user_id = ? and resource=? order by date DESC limit 30';

        $query = $db->query($sql, [$user_id, $name]);



        $result = $query->getResult();

        return  $result;
    }

    public function search($word)
    {
        $db = db_connect();

        $sql = "SELECT *  FROM all_news  WHERE title LIKE '%$word%' OR '$word%' OR '%$word' ";

        $query = $db->query($sql);

        $result = $query->getResult();

        return  $result;
    }

    public function hashtag()
    {
        $db = db_connect();

        $sql = "SELECT DISTINCT categories  FROM all_news  limit 6 ";

        $query = $db->query($sql);

        $result = $query->getResult();

        return  $result;
    }
    public function loadHashtag($hashtag)
    {
        $db = db_connect();

        $sql = "SELECT * FROM all_news Where categories='$hashtag'";

        $query = $db->query($sql);

        $result = $query->getResult();

        return  $result;
    }
}
