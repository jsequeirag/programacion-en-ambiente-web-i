<?php
function conexionEstudiantes()
{
    $link = 'mysql:host=localhost;dbname=student';
    $usuario = 'root';
    $pass = 'root';

    try {

        $pdo = new PDO($link, $usuario, $pass);
        echo 'Conectado<br>';
    } catch (PDOException $e) {
        print "Error!" . $e->getMessage() . "<br>";
        die();
    }
    return $pdo;
}
