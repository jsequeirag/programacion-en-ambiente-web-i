  <?php
    session_start();

    $user = $_SESSION['user'];
    if (!$user) {
        header('Location: index.php');
    } else {
        if ($user['roll'] == 'administrador') {
            header('Location:matricular.php');
        } else {
            $cedulaEnviada = $user['cedula'];
            $contrasenniaEnviada = $user['contrasennia'];
            $correoEnviada = $user['correo'];
            $nombreEnviada = $user['nombre'];
            $apellidosEnviada = $user['apellidos'];
            header("location:inicio.php?cedula=$cedulaEnviada&contrasennia=$contrasenniaEnviada&correo=$correoEnviada&nombre=$nombreEnviada&apellidos=$apellidosEnviada");
        }
    }
    ?>

  <h1> Bienvenido <?php echo $user['name'];
                    echo $user['lastname'] ?> </h1>
  <a href="logout.php">Logout</a>

  <nav class="nav">
      <?php if ($user['role'] === 'Administrador') { ?>
          <li class="nav-item">
              <a class="nav-link active" href="#">Users</a>
          </li>
      <?php } ?>
      <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Arboles</a>
      </li>
  </nav>