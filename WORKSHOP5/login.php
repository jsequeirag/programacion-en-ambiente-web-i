<?php
include_once 'funciones.php';


if ($_POST) {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];

    $user = autentificar($username, $password);
    if ($user) {
        session_start();
        $_SESSION['user'] = $user;
        if ($user['roll'] == 'administrador') {
            header('Location:matricular.php');
        } else {
            $cedulaEnviada = $user['cedula'];
            $contrasenniaEnviada = $user['contrasennia'];
            $correoEnviada = $user['correo'];
            $nombreEnviada = $user['nombre'];
            $apellidosEnviada = $user['apellidos'];
            header("location:inicio.php?cedula=$cedulaEnviada&contrasennia=$contrasenniaEnviada&correo=$correoEnviada&nombre=$nombreEnviada&apellidos=$apellidosEnviada");
        }
    } else {
        header('Location: index.php?status=login');
    }
}
