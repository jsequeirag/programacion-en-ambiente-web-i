<?php
function conexionCover()
{
  $link = 'mysql:host=localhost;dbname=cover';
  $usuario = 'root';
  $pass = '12345678';

  try {

    $pdo = new PDO($link, $usuario, $pass);
    echo 'Conectado<br>';
  } catch (PDOException $e) {
    print "Error!" . $e->getMessage() . "<br>";
    die();
  }
  return $pdo;
}


function loginUser($email, $password)
{

  $sql_unico = 'SELECT * FROM user WHERE (email=? AND password=?)';
  $gsent_unico = conexionCover()->prepare($sql_unico);
  $gsent_unico->execute(array($email, $password));
  $resultado_unico = $gsent_unico->fetch();
  return $resultado_unico;
}


function logout(){
  session_start();
  session_destroy();
  header('Location:index.php');

}

function getAllResourcesUrl($userId)
{

  $query = 'SELECT url,category FROM user_resource WHERE user_id = ?';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;

}