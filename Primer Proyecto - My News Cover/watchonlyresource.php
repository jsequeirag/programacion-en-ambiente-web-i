<?php

include_once 'watchonlyresoursefunc.php';

session_start();

$user = $_SESSION['user'];
$id = $user['id'];
$firstName = $user['first_name'];
$lastName = $user['last_name'];

$resource = $_GET['url'];
$name  = $_GET['name'];


$loadAllNewsByResource = loadAllNewsByResource ($id, $resource);



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <link rel="stylesheet" href="watchonlyresource.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 HEADER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <header>
        <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="index.php">
                <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
            </a>
            <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
                <ul class="navbar-nav me-5">

                    <div class="btn-group me-4">
                        <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
                            <?php echo $firstName = $user['first_name'];
                            echo ' ';
                            echo $lastName = $user['last_name']; ?>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">
                            <li><a class="dropdown-item text-center " href="newresource.php">New Resource</a></li>
                            <li><a class="dropdown-item text-center " href="index.php?action=logout">Log Out</a></li>

                        </ul>

                    </div>

                </ul>
            </div>
        </nav>
    </header>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                  MAIN                                   -->
    <!-- ----------------------------------------------------------------------- -->

    <main>

        <div class="container-tab rounded border mt-4 mb-4">

            <ul class="nav nav-tabs nav-fill mb-3 border-bottom" id="ex1" role="tablist">

                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="main1" data-mdb-toggle="tab" href="#main" role="tab" aria-controls="ex2-tabs-2" aria-selected="false"><?php echo $name ?></a>
                </li>


            </ul>
            <!-- Tabs navs -->

            <!-- Tabs content -->
            <div class="tab-content" id="ex2-content">



                <div class="tab-pane fade show active" id="main" role="tabpanel">
                <div class="float-start d-flex flex-column ms-3 ">
                        <a href="newresource.php">
                            <button type="button" class="btn btn-secondary ms-3 me-5 mt-1 w-75">
                                New Resource
                            </button>
                        </a>



                        <a href="cover.php?action=load">
                            <button type="button" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                                Back
                            </button>
                        </a>
                    </div>


                    <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example">
                        <?php
                        foreach ($loadAllNewsByResource as $new) :
                        ?>
                            <div class="card border mb-1">

                                <?php
                                if (substr($new['img_url'], -3) == 'mp4') :
                                ?>
                                    <img src="http://rafikisafari.com/wp/wp-content/themes/salient/img/no-video-img.png" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                else :
                                ?>
                                    <img src="<?php echo $new['img_url'] ?>" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                endif;
                                ?>
                                <div class="card-body mt-1 ms-1">
                                    <h5 class="card-title"> <?php echo $new['title'] ?></h5>
                                    <h6 class="card-title">Category: <?php echo $new['categories'] ?></h6>
                                    <h7 class="card-title">Source: <?php echo $new['source_name'] ?></h7>
                                    <br>
                                    <h8 class="card-title">Date: <?php echo $new['date'] ?></h8>
                                    <p class="card-text">
                                        <?php echo $new['description'] ?>
                                    </p>
                                    <a href="<?php echo $new['permalink'] ?>" class="btn btn-secondary">Visit Website</a>
                                </div>
                            </div>



                        <?php
                        endforeach;
                        ?>

                    </div>

                </div>

            </div>


        </div>


        </div>
    </main>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 FOOTER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <footer class="bg-light text-center text-lg-start">
        <!-- Grid container -->
        <div class="container p-4">
            <!--Grid row-->
            <div class="row">
                <!--Grid column-->
                <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                    <h5 class="text-uppercase">Footer Content</h5>

                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste
                        atque ea quis molestias. Fugiat pariatur maxime quis culpa
                        corporis vitae repudiandae aliquam voluptatem veniam, est atque
                        cumque eum delectus sint!
                    </p>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                    <h5 class="text-uppercase">Links</h5>

                    <ul class="list-unstyled mb-0">
                        <li>
                            <a href="#!" class="text-dark">Link 1</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 2</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 3</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 4</a>
                        </li>
                    </ul>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                    <h5 class="text-uppercase mb-0">Links</h5>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#!" class="text-dark">Link 1</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 2</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 3</a>
                        </li>
                        <li>
                            <a href="#!" class="text-dark">Link 4</a>
                        </li>
                    </ul>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
            © 2020 Copyright:
            <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- MDB -->

    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

</body>


</html>