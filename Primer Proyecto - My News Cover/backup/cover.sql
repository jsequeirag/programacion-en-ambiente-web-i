-- MySQL dump 10.13  Distrib 5.7.24, for Win32 (AMD64)
--
-- Host: localhost    Database: cover
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `all_news`
--

DROP TABLE IF EXISTS `all_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `all_news` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `resource` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `img_url` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15799 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `all_news`
--

LOCK TABLES `all_news` WRITE;
/*!40000 ALTER TABLE `all_news` DISABLE KEYS */;
INSERT INTO `all_news` VALUES (15783,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','¿Interesado en reforestar? ICE abre recepción de solicitudes para donar árboles','(CRHoy.com). – El Instituto Costarricense de Electricidad (ICE), inició la recepción de solicitudes para la donación forestal de este 2021. La institución cuenta con tres viveros en Cachí, La...','https://www.crhoy.com/?p=1508564','2021-03-15 03:31:26','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Ambiente'),(15784,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Bomberos llevan 4 horas tratando de controlar incendio de charral','(CRHoy.com). -El Cuerpo de Bomberos cumplió a las 9 p. m. 4 horas tratando de controlar un incendio “extenso” en un charral al sur del país. El hecho se registra en Pérez Zeledón, en barrio...','https://www.crhoy.com/?p=1508846','2021-03-15 03:10:05','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15785,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Bad Bunny se deja Grammy por el que competía Debi Nova','(CRHoy.con). -El artista puertorriqueño Bad Bunny fue el ganador en la categoría “Mejor Álbum Pop o Urbano Latino”, en los Grammy 2021, con su producción “YHLQMDLG”. En esta misma...','https://www.crhoy.com/?p=1508841','2021-03-15 03:03:01','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Entretenimiento'),(15786,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','El mono Burgos es el nuevo técnico de Newell’s Old Boys','(CRHoy.com) Germán Burgos, mejor conocido como el “mono”, fue anunciado este domingo como nuevo técnico de Newell’s Old Boys. El equipo rosarino hizo el anuncio mediante sus redes sociales,...','https://www.crhoy.com/?p=1508625','2021-03-15 02:23:07','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Deportes'),(15787,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Decomisan 12.800 cigarros sin facturas','(CRHoy.com). -Oficiales de la Policía de Control Fiscal decomisaron 12.800 cigarrillos este fin de semana, que en apariencia eran transportados en condición irregular. Los hechos se registraron en...','https://www.crhoy.com/?p=1508624','2021-03-15 02:21:36','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15788,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Debi Nova se luce en los Grammy: “Todavía no puedo creer que estoy aquí”','(CRHoy.com). -La cantautora costarricense Debi Nova desfiló por la alfombra roja de los premios Grammy 2021 vestida de rosa y morado la noche de este domingo 14 de marzo. Lo hizo como la primera...','https://www.crhoy.com/?p=1508836','2021-03-15 02:06:49','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Cultura'),(15789,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Detienen a supuesto grupo dedicado a la venta de droga','(CRHoy.com) La Policía Control de Drogas (PCD) del Ministerio de Seguridad Pública desarticuló a supuesto grupo organizado dedicado al tráfico de droga. Los integrantes del grupo fueron...','https://www.crhoy.com/?p=1508657','2021-03-15 01:49:36','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15790,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Cárceles acumulan 20 casos activos de COVID-19; 13 han muerto','(CRHoy.com). -20 privados de libertad están con COVID-19, según el más reciente reporte del Ministerio de Justicia y Paz. Hasta este domingo 14 de febrero se contabilizaban 2.818 recuperados y no...','https://www.crhoy.com/?p=1508645','2021-03-15 01:28:30','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15791,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Lewandowski acecha récord histórico de goleo en la Bundesliga','(CRHoy.com) AFP- El polaco Robert Lewandowski, autor de uno de los goles en la victoria del Bayern Múnich frente al Werder Bremen el sábado (3-1), suma 32 goles en 25 partidos y se acerca al...','https://www.crhoy.com/?p=1508640','2021-03-15 01:11:50','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Deportes'),(15792,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Adulto mayor murió tras accidentarse en moto','(CRHoy.com). -Un adulto mayor de 66 años, identificado como Willian Barrantes Rodríguez, murió la noche del sábado 13 de marzo tras accidentarse en su moto. El reporte del Organismo de...','https://www.crhoy.com/?p=1508764','2021-03-15 00:47:42','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15793,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Propuso ley contra vacunación y murió por COVID-19','(CRHoy.com) Un diputado regional de Brasil que propuso un proyecto de ley en contra la campaña de vacunación en su país, murió por COVID-19. Se trata de Silvio Antonio Favero de 54 años, un...','https://www.crhoy.com/?p=1508490','2021-03-15 00:26:16','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Mundo'),(15794,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Colegios técnicos podrán implementar agricultura de precisión','(CRHoy.com). &#8211; Estudiantes de 18 Colegios Técnicos Profesionales del país se beneficiarán con equipo tecnológico que favorecerá su formación profesional en las especialidades técnicas y...','https://www.crhoy.com/?p=1508759','2021-03-15 00:07:41','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Educación'),(15795,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Pérez y Santos bajan el telón de la jornada con un empate','(CRHoy.com) El Municipal Pérez Zeledón y el Santos de Guápiles bajaron el telón de la Jornada 13, con un empate de 1-1, en el Valle de El General. La primera anotación del compromiso fue obra de...','https://www.crhoy.com/?p=1508820','2021-03-15 00:04:50','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Deportes'),(15796,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Especialistas: Los costarricenses tienen pésimos hábitos al defecar','(CRHoy,com) Investigaciones de población indican que los problemas al defecar son muy comunes en la población costarricense. El estreñimiento, las hemorroides constantes y molestias...','https://www.crhoy.com/?p=1508794','2021-03-14 23:49:48','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Nacionales'),(15797,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','(Video) Michael Barrantes denunció que árbitro lo retó a pelear en la calle','(CRHoy.com) Una vez terminado el juego entre Jicaral y Saprissa, el volante Michael Barrantes hizo una denuncia que de seguro traerá repercusiones. Y es que Barrantes aseguró que el línea Ricardo...','https://www.crhoy.com/?p=1508824','2021-03-14 23:42:15','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Deportes'),(15798,'5','Crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','Drew Brees dice adiós a la NFL: “es hora de que me retire”','(CRHoy.com) AFP- El estelar mariscal de campo de los New Orleans Saints, Drew Brees, anunció su retiro de la NFL este domingo después de una prolífica carrera de 20 años que incluyó un triunfo...','https://www.crhoy.com/?p=1508813','2021-03-14 23:28:49','http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png','Deportes');
/*!40000 ALTER TABLE `all_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('Deportes'),('General'),('Crhoy');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `rol` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'','','','',''),(2,'','','','',''),(3,'1','2','3@3','41235678','user'),(4,'1','2','3@3','1234567789','user'),(5,'Jose','Sequeira','joseluissequeirag@live.com','12345678','user'),(6,'asd','asdasd','joseluissequeirag@live.com','12345678','user'),(7,'r','r','r','r','user'),(8,'r','r','r','r','user'),(9,'r','r','r','r','user'),(10,'r','r','r','r','user'),(11,'r','r','r','r','user'),(12,'r','r','r','r','user'),(13,'r','r','r','r','user'),(14,'r','r','r','r','user'),(15,'r','r','r','r','user'),(16,'r','r','r','r','user'),(17,'r','r','r','r','user'),(18,'r','r','r','r','user'),(19,'r','r','r','r','user'),(20,'r','r','r','r','user'),(21,'r','r','r','r','user'),(22,'r','r','r','r','user'),(23,'r','r','r','r','user'),(24,'Admi','Admi','admi@live.com','12345678','admi');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_resource`
--

DROP TABLE IF EXISTS `user_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_resource`
--

LOCK TABLES `user_resource` WRITE;
/*!40000 ALTER TABLE `user_resource` DISABLE KEYS */;
INSERT INTO `user_resource` VALUES (1,'Deporte','El Paies','1','1'),(2,'2','2','2','2'),(3,'1','Deportes','1','1'),(4,'2','2','2','2'),(9,'Crhoy','crhoy','https://feeds.feedburner.com/crhoy/wSjk?format=xml','5');
/*!40000 ALTER TABLE `user_resource` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-15 12:01:14
