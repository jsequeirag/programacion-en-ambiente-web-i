<?php 
function conexionCover()
{
  $link = 'mysql:host=localhost;dbname=cover';
  $usuario = 'root';
  $pass = '12345678';

  try {

    $pdo = new PDO($link, $usuario, $pass);
  } catch (PDOException $e) {
    print "Error!" . $e->getMessage() . "<br>";
    die();
  }
  return $pdo;
}

function loadAllNewsByResource ($userId,$recourse)
{

  $query = 'SELECT * FROM all_news WHERE user_id = ? and resource=? order by date DESC limit 30';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId,$recourse));

  $result = $gsent->fetchAll();

  return $result;
}

?>