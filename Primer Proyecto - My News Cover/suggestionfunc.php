<?php

function conexionCover()
{
  $link = 'mysql:host=localhost;dbname=cover';
  $usuario = 'root';
  $pass = '12345678';

  try {

    $pdo = new PDO($link, $usuario, $pass);
    
  } catch (PDOException $e) {
    print "Error!" . $e->getMessage() . "<br>";
    die();
  }
  return $pdo;
}


function insertResources($user_id, $url,$name, $category){                              
   
   $query = 'INSERT INTO user_resource (user_id, url,name, category) VALUES (?, ?, ?, ?)';
 
   $addSentence = conexionCover()->prepare($query);
 
   $addSentence->execute(array($user_id, $url,$name, $category));

  
 }

 function getCategories(){

  $query = 'SELECT * FROM category';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute();
  
  $result = $gsent->fetchAll();

  return $result;
}