<?php
include_once 'conexion.php';

/* -------------------------------------------------------------------------- */
/*                                    EDITA                                   */
/* -------------------------------------------------------------------------- */

if ($_GET['accion'] == 'confirmada') {
  $categoria = $_GET['categoria'];
  $descripcion = $_GET['descripcion'];
  $id = $_GET['id'];

  echo $categoria, $descripcion;

  $sql_editar = 'UPDATE categoria SET categoria=?,descripcion=? WHERE id=?';
  $sentencia_editar = $pdo->prepare($sql_editar);
  $sentencia_editar->execute(array($categoria, $descripcion, $id));
  echo 'agregado';

  header('location:registro.php');
}



/* -------------------------------------------------------------------------- */
/*                                   INSERTA                                  */
/* -------------------------------------------------------------------------- */

if ($_POST) {

  $categoria = $_POST['categoria'];
  $descripcion = $_POST['descripcion'];


  echo $categoria, $descripcion;

  $sql_agregar = 'INSERT INTO categoria(categoria,descripcion) VALUES (?,?)';
  $sentencia_agregar = $pdo->prepare($sql_agregar);
  $sentencia_agregar->execute(array($categoria, $descripcion));
  echo 'agregado';

  header('location:registro.php');
}

/* -------------------------------------------------------------------------- */
/*                                   elimina                                  */
/* -------------------------------------------------------------------------- */

if ($_GET['accion'] == 'eliminar') {
  $categoria = $_GET['categoria'];
  $descripcion = $_GET['descripcion'];
  $id = $_GET['id'];

  echo $categoria, $descripcion;

  $sql_eliminar = 'DELETE FROM categoria WHERE id=?';
  $sentencia_eliminar = $pdo->prepare($sql_eliminar);
  $sentencia_eliminar->execute(array($id));
  echo 'Eliminado';

  header('location:registro.php');
}
/* -------------------------------------------------------------------------- */
/*                                    carga                                   */
/* -------------------------------------------------------------------------- */

$sql_leer = 'SELECT * FROM categoria';
$gsent = $pdo->prepare($sql_leer);
$gsent->execute();

$resultado = $gsent->fetchAll();
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
  <style>
    main {
      height: 100vh;
      background-color: lightblue;
    }

    div {
      width: 60vw;
      background-color: lightblue;
    }
  </style>
</head>

<body>
  <main class="container d-flex flex-column align-items-center ">
    <div class="row">
      <?php
      if (!$_GET) :
      ?>
        <form method="POST" class="text-center ">
          <h1>categorias</h1>
          <input type="text" class="form-control" name="categoria" placeholder="Categoria" />
          <input type="text" class="form-control" name="descripcion" placeholder="Descripcion" />
          <button class="btn btn-primary mt-3">Registrar</button>
        </form>
      <?php
      endif
      ?>

      <?php
      if ($_GET['accion'] == 'editar') :
        echo 'editar'
      ?>
        <form method="GET" class="text-center ">
          <h1>categorias</h1>
          <input type="text" class="form-control" name="categoria" placeholder="Categoria" value="<?php echo $_GET['categoria'] ?>" />
          <input type="text" class="form-control" name="descripcion" placeholder="Descripcion" value="<?php echo $_GET['descripcion'] ?>" />
          <input type="text" class="form-control" hidden name="id" value="<?php echo $_GET['id'] ?>">
          <input type="text" class="form-control" hidden name="accion" value="confirmada">
          <button class="btn btn-primary mt-3">editar</button>
        </form>
      <?php
      endif
      ?>

    </div>
    <div class="row">

      <?php
      foreach ($resultado as $dato) :
      ?>
        <div class="col-2 border">
          <p>Categoria:<?php echo $dato['categoria'] ?></p>
        </div>
        <div class="col-8 border">
          <p>Descripcion:<?php echo $dato['descripcion'] ?> </p>
        </div>
        <div class="col-2 border">
          <a class="btn btn-primary" href="registro.php?categoria=<?php echo $dato['categoria'] ?>&descripcion=<?php echo  $dato['descripcion'] ?>&id=<?php echo $dato['id'] ?>&accion=editar">editar</a>
          <a class="btn btn-primary" href="registro.php?id=<?php echo $dato['id'] ?>&accion=eliminar">eliminar</a>
        </div>

      <?php
      endforeach
      ?>


    </div>

  </main>
</body>

</html>