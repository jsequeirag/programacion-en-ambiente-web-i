<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
    <style>
        main {
            height: 100vh;
            background-color: lightblue;

        }
    </style>
</head>

<body>
    <main class="container d-flex align-items-center justify-content-center">
        <div>
            <form method="GET" class="text-center">

                <a href="matricular.php" class="btn btn-primary  mt-3">Matricular</a>
                <a href="registro.php" class="btn btn-primary  mt-3">Registros</a>

            </form>
        </div>
    </main>
</body>

</html>