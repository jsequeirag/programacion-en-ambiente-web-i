<?php
include_once 'conexion.php';
$sql_leermatricula = 'SELECT * FROM matricula';
$gsentmatricula = $pdo->prepare($sql_leermatricula);
$gsentmatricula->execute();
$resultadoMatriculas = $gsentmatricula->fetchAll();


$sql_leercarrera = 'SELECT * FROM carrera';
$gsentcarrera = $pdo->prepare($sql_leercarrera);
$gsentcarrera->execute();
$resultadoCarreras = $gsentcarrera->fetchAll();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
</head>

<body>
    <main class="container d-flex  flex-column ">
        <?php
        foreach ($resultadoMatriculas as $matricula) :
        ?>
            <div class="row border">

                <div class="col-2 border">
                    <?php
                    echo 'Nombre:', $matricula['nombre'];
                    ?>
                </div>
                <div class="col-2 border">
                    <?php
                    echo 'Apellido:', $matricula["apellido"];
                    ?>
                </div>
                <div class="col-2 border">
                    <?php
                    echo 'Cedula:', $matricula["cedula"];
                    ?>
                </div>
                <div class="col-3 border">
                    <?php
                    echo 'Correo', $matricula["correo"];
                    ?>
                </div>
                <div class="col-3 border">
                    <?php
                    foreach ($resultadoCarreras as $carrera) {
                        if ($carrera['id'] == $matricula["id_carrera"]) {

                            echo 'Carrera:', $carrera["nombre_carrera"];
                        }
                    }
                    ?>

                </div>

            </div>

        <?php
        endforeach;
        ?>
    </main>

</body>

</html>