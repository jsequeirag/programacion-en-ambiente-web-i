<?php
include_once 'functions.php';
session_start();

$user = $_SESSION['user'];
$id = $user['id'];
$firstName = $user['first_name'];
$lastName = $user['last_name'];



if ($_GET['action'] == 'insert') {
  $categoryName = $_GET['categoryname'];

  $existenciaCategoria = getOnlyCategory($categoryName);

  if ($existenciaCategoria) {
    $message = '12312';
    header("location:addcategory.php?categoryname=$categoryName&message=Category existed");
  } else {

    insertCategory($categoryName);
  }
}
if ($_GET['action'] == 'sentedit') {
  $newcategoryName = $_GET['newcategoryname'];
  $oldcategoryName = $_GET['oldcategoryname'];
  $existenciaCategoria = getOnlyCategory($newcategoryName);

  if ($existenciaCategoria) {
    $message = '12312';
    header("location:addcategory.php?categoryname=$newcategoryName&action=edit&message=Category existed");
  } else {

    include_once 'functions.php';
    updateCategory($oldcategoryName, $newcategoryName);
    header("location:edit.php");
    echo 'no existe';
  }
}

if ($_GET['action'] == 'edit') {
  $categoryName = $_GET['categoryname'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="addcategory.css" />

  <link rel="stylesheet" href="addcategory.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />


</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
              <?php echo $firstName = $user['first_name'];
              echo ' ';
              echo $lastName = $user['last_name']; ?>
            </button>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">
              <li><a class="dropdown-item text-center " href="newresource.php">New Resource</a></li>
              <li><a class="dropdown-item text-center " href="index.php?action=logout">Log Out</a></li>

            </ul>

          </div>

        </ul>
      </div>
    </nav>
  </header>


  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>
    <div class="container-form">



      <form method="GET" id="form" class="border p-5 border-secondary rounded">
        <?php
        if ($_GET['message'] == 'Category existed') :
        ?>
          <p class="text-center text-danger"><?php echo 'Category existed' ?></p>
        <?php endif ?>
        <?php
        if ($_GET["action"] == "edit") :
        ?>
          <div class="form-outline mb-4">
            <input type="text" class="form-control" name="newcategoryname" value="<?php echo $categoryName; ?>" required="required" />
            <input type="text" class="form-control" name="oldcategoryname" value="<?php echo $categoryName; ?>" hidden />
            <input type="text" class="form-control" name="action" value="sentedit" hidden />
            <label class="form-label" for="form3Example3">Name</label>
          </div>

          <!-- Submit button -->
          <button type="submit" class="btn btn-primary btn-block mb-4 Width 50%">Guardar</button>


          <a href="edit.php"><button type="button" class=" btn btn-warning btn-block mb-4">Back</button></a>

          <!-- Back buttons -->


        <?php
        else :
        ?>
          <?php
          if ($_GET['message'] == 'Category existed') :
          ?>
          <?php endif ?>
          <div class="form-outline mb-4">
            <input type="text" class="form-control" name="categoryname" required="required" />
            <input type="text" class="form-control" name="action" value="insert" hidden />
            <label class="form-label" for="form3Example3">Name</label>
          </div>

          <!-- Submit button -->
          <button type="submit" class="btn btn-primary btn-block mb-4">Guardar</button>

          <!-- Back buttons -->
          <a href="cover.php">
          <a href="edit.php"><button type="button" class=" btn btn-warning btn-block mb-4">Back</button></a>
          </a>
        <?php
        endif;
        ?>
      </form>
    </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="bg-light text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase">About</h5>

          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque
            ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae
            repudiandae aliquam voluptatem veniam, est atque cumque eum delectus
            sint!
          </p>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Devices</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-dark">PC</a>
            </li>
            <li>
              <a href="#!" class="text-dark">iOS</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Android</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Social media</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-dark">Fcebook</a>
            </li>
            <li>
              <a href="#!" class="text-dark">twitter</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Diaspora</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2020 Copyright:
      <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>