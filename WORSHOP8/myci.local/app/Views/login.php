

<!-- ----------------------------------------------------------------------- -->
<!--                                  HTML                                   -->
<!-- ----------------------------------------------------------------------- -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="login.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <a href="register.php">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Register
              </button>
            </a>

          </div>

        </ul>
      </div>
    </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>


    <div class="container-form mt-1 mb-1">
      <form method="GET" action='user/print' class="border p-5 border-secondary rounded">
        
        <!-- Email input -->
        <div class="form-outline mb-4">
          <input type="text" id="form2Example1" class="form-control" name="email" required />

          <label class="form-label" for="form2Example1">Email address</label>
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
          <input type="password" id="form2Example2" class="form-control" name="password" required />
          <label class="form-label" for="form2Example2">Password</label>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-secondary btn-block mb-4">Sign in</button>

        <div class="col">
          <!-- Simple link -->
          <p class="text-center">if you dont have account,
            <a text-center href="register.php">signup here</a>
          </p>
        </div>
      </form>
    </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="bg-light text-center text-lg-start">
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
        <h5 class="text-uppercase">About</h5>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque
          ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae
          repudiandae aliquam voluptatem veniam, est atque cumque eum delectus
          sint!
        </p>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Devices</h5>
     
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-dark">PC</a>
          </li>
          <li>
            <a href="#!" class="text-dark">iOS</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Android</a>
          </li>

        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase mb-0">Social media</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-dark">Fcebook</a>
          </li>
          <li>
            <a href="#!" class="text-dark">twitter</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Diaspora</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © 2020 Copyright:
    <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  JAVASCRIPT                             -->
  <!-- ----------------------------------------------------------------------- -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  JAVASCRIPT                             -->
  <!-- ----------------------------------------------------------------------- -->
  HTML
</body>

</html>