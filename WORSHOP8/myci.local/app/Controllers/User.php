<?php

namespace App\Controllers;

class User extends BaseController
{
	public function index()
	{
		return view('login');
	}

	public function dashboard()
	{
		return view('dashboard');
	}

	public function edit()
	{
		return view('edit');
	}


	public function getAllUsers()
	{
		$userModel = new \App\Models\UserModel();

		$allUsers = $userModel->getAllUsers();

		return $allUsers;
	}

	public function delete($userName)
	{

		echo $userName;



		$userModel = new \App\Models\UserModel();

		$deleteUser = $userModel->deleteUser($userName);

		return $deleteUser;
		header('Location:dashboard');
		exit();
	}

	public function editaction($old)
	{
		echo $old;
		// header('Location:/user/edit');

		$data = [
			'usuario' => "$old"
		];
		echo view('edit',$data);
	}

	public function editConfirm()
	{

		$old= $this->request->getGet('old');
		$new=$this->request->getGet('new');

		$userModel = new \App\Models\UserModel();

		$editUser = $userModel->editUser($new,$old);

		header('Location:dashboard');
		//exit();

		

	}

	public function print()
	{
		$email = $this->request->getGet('email');

		$password = $this->request->getGet('password');



		$userModel = new \App\Models\UserModel();

		$user = $userModel->login($email, $password);


		if ($user) {
			header('Location:dashboard');
			exit();
		} else {

			header("Location:index");
			exit();
		}
	}
}
